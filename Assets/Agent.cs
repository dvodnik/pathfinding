﻿using UnityEngine;

public enum AgentState
{
    PathFinding,
    Driving,
    Manual,
    Paused,
    Idle
};

public class Agent : MonoBehaviour
{

    public Vector2 start;
    public Vector2 end;

    public AgentState state;

    public PathFinder pathfinder;
    public Driver driver;
    public Mapper mapper;

    // Use this for initialization
    void Start()
    {
        mapper = new Mapper();
        state = AgentState.Idle;
        LaserScanner laserscanner = GameObject.Find("LaserScanner").GetComponent(typeof(LaserScanner)) as LaserScanner;
        Ackermann ackermann = GameObject.Find("Ackermann").GetComponent(typeof(Ackermann)) as Ackermann;
        ackermann.transform.position = new Vector3(start.x, start.y, 0);
        pathfinder = new PathFinder(ackermann);
        driver = new Driver(ackermann);
        laserscanner.mapper = mapper;
    }

    // Update is called once per frame
    void Update()
    {
        start:
        switch (state)
        {
            case AgentState.PathFinding:
                driver.stop();
                search();
                break;
            case AgentState.Driving:
                if (pathfinder.mapper.pathCollision)
                {
                    state = AgentState.PathFinding;
                    pathfinder.done = false;
                    pathfinder.mapper.pathCollision = false;
                    Debug.Log("collision caught");
                    goto start;
                }
                bool goalReached = driver.drive();
                if (goalReached) state = AgentState.Idle;
                break;
            case AgentState.Paused:
                driver.stop();
                break;
            case AgentState.Idle:
                driver.stop();
                break;
        }
    }

    private void search()
    {
        if (pathfinder.done == true)
        {
            state = AgentState.Driving;
            driver.setPath(pathfinder.referencePath);
            pathfinder.done = false;
        }
        else if (!pathfinder.executing) StartCoroutine(pathfinder.FindPath(mapper, end));
    }
}