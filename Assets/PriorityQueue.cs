using System;
using System.Collections.Generic;
using UnityEngine;

public class pathNode : IComparable<pathNode>
{
    public Position position;
    public float steering;
    public bool reverse;
    public float cost;
    public float heuristic;
    public pathNode parent;

    public int CompareTo(pathNode other)
    {
        if (this.heuristic < other.heuristic) return -1;
        else if (this.heuristic > other.heuristic) return 1;
        else return 0;
    }

    public struct key
    {
        public int x, y;
        public int h;
        public bool r;
    };


    float position_cells = 1;
    int heading_cells = 8;
    bool reverse_cell = true;
    public key getkey()
    {
        key a = new key();
        a.x = (int)(position.position.x*position_cells);
        a.y = (int)(position.position.y*position_cells);
        a.h = (int)(heading_cells * position.heading / Mathf.PI);
        a.r = reverse & reverse_cell;
        return a;
    }

    public pathNode(float pc, int hc, bool rc)
    {
        position_cells = pc;
        heading_cells = hc;
        reverse_cell = rc;
    }
}


public class PriorityQueue<T> where T : IComparable<T>
{
    private List<T> data;

    public PriorityQueue()
    {
        this.data = new List<T>();
    }

    public void Add(T item)
    {
        data.Add(item);
        int ci = data.Count - 1;
        while (ci > 0)
        {
            int pi = (ci - 1) / 2;
            if (data[ci].CompareTo(data[pi]) >= 0)
                break;
            T tmp = data[ci]; data[ci] = data[pi]; data[pi] = tmp;
            ci = pi;
        }
    }

    public T Pop()
    {
        if (data.Count == 0)
            return default(T);
        int li = data.Count - 1;
        T frontItem = data[0];
        data[0] = data[li];
        data.RemoveAt(li);

        --li;
        int pi = 0;
        while (true)
        {
            int ci = pi * 2 + 1;
            if (ci > li) break;
            int rc = ci + 1;
            if (rc <= li && data[rc].CompareTo(data[ci]) < 0)
                ci = rc;
            if (data[pi].CompareTo(data[ci]) <= 0) break;
            T tmp = data[pi]; data[pi] = data[ci]; data[ci] = tmp;
            pi = ci;
        }
        return frontItem;
    }
}
