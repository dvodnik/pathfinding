﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinder
{
    List<pathNode> expanded;
    PriorityQueue<pathNode> frontier;
    Dictionary<pathNode.key, int> opened;

    public Mapper mapper;

    pathNode expand(Vector2 end)
    {
        pathNode best_tree = null;
        while (true)
        {
            best_tree = frontier.Pop();
            if (best_tree == null)
                return null;
            if (opened.ContainsKey(best_tree.getkey()))
                continue;
            if (mapper.collides(best_tree))
            {
                continue;
            }
            break;
        }
        opened.Add(best_tree.getkey(), 0);
        expanded.Add(best_tree);
        return best_tree;
    }

    pathNode goalNode;

    public int heu_type = 2;

    int begx, begy;
    int[,] heuristicsMap;
    float maxHeuristics = 1;
    public void CreateHeuristics(Mapper mapper, Vector2 end)
    {
        float startx = end.x;
        float starty = end.y;
        float endx = startx, endy = starty;

        if (ackermann.transform.position.x < startx) startx = ackermann.transform.position.x;
        if (ackermann.transform.position.y < starty) starty = ackermann.transform.position.y;
        if (ackermann.transform.position.x > endx) endx = ackermann.transform.position.x;
        if (ackermann.transform.position.y > endy) endy = ackermann.transform.position.y;

        for (int i = 0; i < mapper.walls.Count; ++i)
        {
            Vector3 wpos = mapper.walls[i].transform.position;
            if (wpos.x < startx) startx = wpos.x;
            if (wpos.y < starty) starty = wpos.y;
            if (wpos.x > endx) endx = wpos.x;
            if (wpos.y > endy) endy = wpos.y;
        }
        startx -= 5;
        starty -= 5;
        endx += 5;
        endy += 5;
        begx = (int)startx;
        begy = (int)starty;
        int sizex = (int)endx - (int)startx;
        int sizey = (int)endy - (int)starty;
        heuristicsMap = new int[sizex, sizey];
        Queue<Vector3> queue = new Queue<Vector3>();
        Dictionary<Vector2, bool> opened = new Dictionary<Vector2, bool>();

        int ex = (int)end.x;
        int ey = (int)end.y;
        queue.Enqueue(new Vector3(ex, ey, 0));
        opened.Add(new Vector3(ex, ey), false);
        while (queue.Count != 0)
        {
            Vector3 ni = queue.Dequeue();
            int x = (int)ni.x;
            int y = (int)ni.y;
            int cost = (int)ni.z;
            if (heu_type == 0)
            {
                heuristicsMap[x - begx, y - begy] = 0;
            } else if (heu_type == 1)
            {
                heuristicsMap[x - begx, y - begy] = (int)Mathf.Abs(x - end.x) + (int)Mathf.Abs(y - end.y);
            } else
            {
                heuristicsMap[x - begx, y - begy] = cost;
            }
            //heuristicsMap[x - begx, y - begy] = (int)Mathf.Abs(x - end.x) + (int)Mathf.Abs(y - end.y);
            if (heuristicsMap[x - begx, y - begy] > maxHeuristics) maxHeuristics = cost;
            for (int xx = -1; xx <= 1; ++xx)
            {
                for (int yy = -1; yy <= 1; ++yy)
                {
                    //if (xx == yy) continue;
                    if (xx != 0) if (yy != 0) continue;
                    int newx = x + xx;
                    int newy = y + yy;
                    int newcost = cost + 1;
                    if (opened.ContainsKey(new Vector2(newx, newy))) continue;
                    if (newx < begx) continue;
                    if (newx >= begx + sizex) continue;
                    if (newy < begy) continue;
                    if (newy >= begy + sizey) continue;
                    if (heu_type == 2)
                    {
                        heuristicsMap[newx - begx, newy - begy] = cost; // this is a grow operation towards obstacles, but it may look a bit out of place (this whole function needs a rewrite)
                    }
                    else if (heu_type == 1)
                    {
                        heuristicsMap[newx - begx, newy - begy] = (int)Mathf.Abs(newx - end.x) + (int)Mathf.Abs(newy - end.y);
                    }
                    bool c = false;
                    for (int i = 0; i < mapper.walls.Count; ++i)
                    {
                        Vector3 wallpos = mapper.walls[i].transform.position;
                        if (wallpos.x >= newx-1) if (wallpos.x <= newx + 2) if (wallpos.y >= newy-1) if (wallpos.y <= newy + 2)
                                    {
                                        c = true;
                                        break;
                                    }
                    }
                    if (c) continue;
                    queue.Enqueue(new Vector3(newx, newy, newcost));
                    opened.Add(new Vector2(newx, newy), false);
                }
            }
        }
    }

    public int show_steps = 100;

    public float position_cells = 1;
    public int heading_cells = 8;
    public bool reverse_cell = true;

    public bool executing = false;
    public bool done = false;
    public IEnumerator FindPath(Mapper mapper, Vector2 end)
    {
        this.mapper = mapper;
        CreateHeuristics(mapper, end);
        executing = true;
        done = false;
        expanded = new List<pathNode>();
        frontier = new PriorityQueue<pathNode>();
        opened = new Dictionary<pathNode.key, int>();
        finalPath = new List<Path>();
        goalNode = null;
        pathNode root_tree = new pathNode(position_cells, heading_cells, reverse_cell);
        root_tree.position = new Position(ackermann.transform.position.x, ackermann.transform.position.y, ackermann.transform.eulerAngles.z * Mathf.Deg2Rad);
        root_tree.cost = 0;
        root_tree.heuristic = heuristicsMap[(int)root_tree.position.position.x - begx, (int)root_tree.position.position.y - begy];
        frontier.Add(root_tree);
        for (int i = 0; i < 100000; ++i)
        {
            pathNode parent = expand(end);
            if (parent == null) break;
            if (Vector2.Distance(parent.position.position, end) < lstep)
            {
                goalNode = parent;
                CreatePath();
                createForward();
                mapper.collisionPath(finalPath);
                executing = false;
                done = true;
                break;
            }
            for (float k = -ackermann.maxSteering; k <= ackermann.maxSteering; k += ackermann.maxSteering / 4)
            {
                foreach (bool reverse in new bool[]{false, true})
                {
                    pathNode a = expandNode(parent, k, reverse, end);
                    frontier.Add(a);
                }
            }

            if (i % show_steps == 0)
            {
                Debug.Log(i);
                yield return new WaitForSeconds(0);
            }
        }
        yield return new WaitForSeconds(1);
        executing = false;
        //done = true;
    }

    public float direction_change_cost = 10;
    public float reverse_cost = 5;
    public float steering_change_cost = 1;
    public float steering_cost = 0;
    private pathNode expandNode(pathNode parent, float steering, bool reverse, Vector2 end)
    {
        pathNode a = new pathNode(position_cells, heading_cells, reverse_cell);
        a.parent = parent;
        a.steering = steering;
        a.reverse = reverse;
        /* */
        a.cost = a.parent.cost;
        if (a.reverse && !a.parent.reverse)
        {
            a.cost += lstep * direction_change_cost;
        }
        else
        {
            if (reverse) a.cost += lstep * reverse_cost;
            else a.cost += lstep;
            a.cost += Mathf.Abs(parent.steering - steering) * lstep * steering_change_cost;
            a.cost += Mathf.Abs(steering) * lstep * steering_cost;
        }

        a.position = parent.position.add(ackermann.nextPosition(steering, reverse ? -lstep : lstep));

        float heuristics;
        int heux = (int)a.position.position.x - begx;
        int heuy = (int)a.position.position.y - begy;
        switch (heu_type)
        {
            case 0:
                heuristics = 0;
                break;
            case 1:
                heuristics = Mathf.Abs(end.x - a.position.position.x) + Mathf.Abs(end.y - a.position.position.y);
                break;
            default:
                if ((heux < 0) || (heuy < 0) || (heux >= heuristicsMap.GetLength(0)) || (heuy >= heuristicsMap.GetLength(1))) heuristics = Mathf.Infinity;
                else heuristics = heuristicsMap[heux, heuy]; //heuristics = 0;
                break;
        }
        a.heuristic = a.cost + heuristics;
        //Debug.LogFormat("{0} {1}", Vector2.Distance(a.position.position, end), heuristicsMap[(int)a.position.position.x - begx, (int)a.position.position.y - begy]);
        return a;    }

    public struct Path
    {
        public bool reverse;
        public List<Vector3> positions;

        public Path(bool reverse, List<Vector3> positions)
        {
            this.reverse = reverse;
            this.positions = positions;
        }
    }
    public float lstep = 1;
    public List<Path> finalPath;
    void CreatePath()
    {
        List<Vector3> partialPath = new List<Vector3>();
        pathNode tmp = goalNode;
        if (tmp == null) return;
        while (true)
        {
            if (tmp.parent == null)
            {
                finalPath.Insert(0, new Path(tmp.reverse, partialPath));
                break;
            }
            for (int i = 0; i < 100; ++i)
            {
                Position headingV = ackermann.nextPosition(tmp.steering, lstep*(1 - (float)i / 100));
                if (tmp.reverse)
                {
                    headingV.position.x = -headingV.position.x;
                }
                float x = headingV.position.x;
                float y = headingV.position.y;
                float sin = Mathf.Sin(tmp.parent.position.heading);
                float cos = Mathf.Cos(tmp.parent.position.heading);
                Vector2 b = tmp.parent.position.position + new Vector2(cos * x - sin * y, sin * x + cos * y);
                Vector3 p = new Vector3(b.x, b.y, 2);
                partialPath.Insert(0, p);
            }
            if (tmp.reverse != tmp.parent.reverse)
            {
                finalPath.Insert(0, new Path(tmp.reverse, partialPath));
                partialPath = new List<Vector3>();
            }
            tmp = tmp.parent;
        }
    }

    public List<Path> referencePath;
    void createForward()
    {
        referencePath = new List<Path>();
        for (int p = 0; p < finalPath.Count; ++p)
        {
            List<Vector3> partialReference = new List<Vector3>();

            List<Vector3> partialPath = finalPath[p].positions;
            for (int i = 0; i < partialPath.Count; ++i)
            {
                int i1 = i == 0 ? 0 : i - 1;
                int i2 = i == partialPath.Count - 1 ? i : i + 1;
                Vector3 v1 = partialPath[i1];
                Vector3 v2 = partialPath[i2];

                float dx = v2.x - v1.x;
                float dy = v2.y - v1.y;

                float heading = Mathf.Atan2(dy, dx);

                float y = Mathf.Sin(heading) * (float)1.5;
                float x = Mathf.Cos(heading) * (float)1.5;

                partialReference.Add(partialPath[i] + new Vector3(x, y, 0));
            }
            referencePath.Add(new Path(finalPath[p].reverse, partialReference));
        }
    }

    public void drawPath(bool dheu = false)
    {
        GL.Begin(GL.QUADS);
        if (executing | dheu)
        {
            if (heuristicsMap != null)
            for (int x = 0; x < heuristicsMap.GetLength(0); ++x)
            {
                for (int y = 0; y < heuristicsMap.GetLength(1); ++y)
                {
                    if (heuristicsMap[x, y] != 0)
                    {
                        float ipl = heuristicsMap[x, y] / maxHeuristics;
                        GL.Color(Color.Lerp(Color.Lerp(Color.green, Color.red, ipl), Color.black, (float)0.5));
                        GL.Vertex3(begx + x, begy + y, 2);
                        GL.Vertex3(begx + x + 1, begy + y, 2);
                        GL.Vertex3(begx + x + 1, begy + y + 1, 2);
                        GL.Vertex3(begx + x, begy + y + 1, 2);
                    }
                }
            }
        }
        GL.End();

        GL.Begin(GL.LINES);
        if (executing)
        {
            if (expanded != null)
                for (int i = 0; i < expanded.Count; ++i)
                {
                    if (i != 0)
                    {
                        Vector3 s = new Vector3(expanded[i].parent.position.position.x, expanded[i].parent.position.position.y, 2);
                        Vector3 e = new Vector3(expanded[i].position.position.x, expanded[i].position.position.y, 2);
                        if (expanded[i].reverse)
                        {
                            GL.Color(Color.black);
                        }
                        else
                        {
                            GL.Color(Color.black);
                        }
                        GL.Vertex3(s.x, s.y, (float)1.5);
                        GL.Vertex3(e.x, e.y, (float)1.5);
                    }
                }
            }
            GL.Color(Color.black);
            if (finalPath != null) if (!executing)
                for (int p = 0; p < finalPath.Count; ++p)
                {
                    List<Vector3> partialPath = finalPath[p].positions;
                    for (int i = 0; i < partialPath.Count - 1; ++i)
                    {
                        Vector3 s = partialPath[i];
                        Vector3 e = partialPath[i + 1];
                        GL.Vertex3(s.x, s.y, (float)1.5);
                        GL.Vertex3(e.x, e.y, (float)1.5);
                    }
                }
            GL.Color(Color.black);
            if (referencePath != null) if (!executing)
                for (int p = 0; p < referencePath.Count; ++p)
                {
                    List<Vector3> partialPath = referencePath[p].positions;
                    for (int i = 0; i < partialPath.Count - 1; ++i)
                    {
                        Vector3 s = partialPath[i];
                        Vector3 e = partialPath[i + 1];
                        GL.Vertex3(s.x, s.y, (float)1.5);
                        GL.Vertex3(e.x, e.y, (float)1.5);
                    }
            }
        GL.End();
    }

    Ackermann ackermann;
    public PathFinder(Ackermann ackermann)
    {
        this.ackermann = ackermann;
    }
}
