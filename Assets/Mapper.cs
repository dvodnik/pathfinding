﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mapper
{
    public List<GameObject> walls;
    private List<PathFinder.Path> finalPath;
    public bool pathCollision = false;

    public void reset()
    {
        walls.Clear();
    }

    public void collisionPath(List<PathFinder.Path> path)
    {
        finalPath = path;
        //pathCollision = false;
    }

    private bool collidesPath(GameObject o)
    {
        if (finalPath != null)
            for (int j = 0; j < finalPath.Count; ++j)
            {
                for (int i = 1; i < finalPath[j].positions.Count; ++i)
                {
                    Vector2 b1, b2;
                    b1 = new Vector2(finalPath[j].positions[i - 1].x, finalPath[j].positions[i - 1].y);
                    b2 = new Vector2(finalPath[j].positions[i].x, finalPath[j].positions[i].y);
                    float dx = b2.x - b1.x;
                    float dy = b2.y - b1.y;
                    float angle = Mathf.Atan2(dy, dx);
                    if (col(finalPath[j].positions[i], angle, o)) return true;
                }
            }
        return false;
    }

    public void AddObstacle(GameObject wall)
    {
        if (wall.layer == 8)
        {
            wall.layer = 9;
            wall.GetComponent<MeshRenderer>().material.color = Color.red;
            walls.Add(wall);
            if (collidesPath(wall)) pathCollision = true;
            if (pathCollision) Debug.Log("collision detected");
        }
    }

    bool linesIntersect(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
    {
        float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));
        float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
        float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

        // Detect coincident lines (has a problem, read below)
        if (denominator == 0) return numerator1 == 0 && numerator2 == 0;

        float r = numerator1 / denominator;
        float s = numerator2 / denominator;

        return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
    }

    public bool col(Vector2 point, float heading, GameObject b)
    {
        Vector3 a1, a2, b1, b2;
        float scale = b.transform.localScale.x;
        a1 = Quaternion.Euler(0, 0, b.transform.rotation.z) * new Vector3(-scale/2, 0, 0) + b.transform.position;
        a2 = Quaternion.Euler(0, 0, b.transform.rotation.z) * new Vector3(scale/2, 0, 0) + b.transform.position;

        Vector3 aa1 = Quaternion.Euler(0, 0, -heading) * (a1 - (Vector3)point);
        Vector3 aa2 = Quaternion.Euler(0, 0, -heading) * (a2 - (Vector3)point);
        
        float back = (float)1.2 + (float).2;
        float side = (float)1 + (float).2;
        float front = (float)2.5 + (float).2;

        if (aa1.x <= front) if (aa1.x >= -back) if (aa1.y <= side) if (aa1.y >= -side) return true;

        float sin = Mathf.Sin(heading);
        float cos = Mathf.Cos(heading);
        b1 = new Vector2(-back, -side);
        b2 = new Vector2(-back, side);
        b1 = new Vector2(cos * b1.x - sin * b1.y, sin * b1.x + cos * b1.y) + point;
        b2 = new Vector2(cos * b2.x - sin * b2.y, sin * b2.x + cos * b2.y) + point;
        if (linesIntersect(a1, a2, b1, b2)) return true;
        b1 = new Vector2(-back, -side);
        b2 = new Vector2(front, -side);
        b1 = new Vector2(cos * b1.x - sin * b1.y, sin * b1.x + cos * b1.y) + point;
        b2 = new Vector2(cos * b2.x - sin * b2.y, sin * b2.x + cos * b2.y) + point;
        if (linesIntersect(a1, a2, b1, b2)) return true;
        b1 = new Vector2(-back, side);
        b2 = new Vector2(front, side);
        b1 = new Vector2(cos * b1.x - sin * b1.y, sin * b1.x + cos * b1.y) + point;
        b2 = new Vector2(cos * b2.x - sin * b2.y, sin * b2.x + cos * b2.y) + point;
        if (linesIntersect(a1, a2, b1, b2)) return true;
        b1 = new Vector2(front, -side);
        b2 = new Vector2(front, side);
        b1 = new Vector2(cos * b1.x - sin * b1.y, sin * b1.x + cos * b1.y) + point;
        b2 = new Vector2(cos * b2.x - sin * b2.y, sin * b2.x + cos * b2.y) + point;
        if (linesIntersect(a1, a2, b1, b2)) return true;
        return false;
    }

    public bool collides(pathNode a)
    {
        for (int w = 0; w < walls.Count; ++w)
        {
            if (col(a.position.position, a.position.heading, walls[w])) return true;
        }
        return false;
    }

    public Mapper()
    {
        walls = new List<GameObject>();
    }
};