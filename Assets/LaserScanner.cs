﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserScanner : MonoBehaviour
{
    public Vector2 laserStart;
    public List<Vector2> laserEnd;

    public Mapper mapper;

    float laserDistance = 100;

    public void Update()
    {
        laserEnd = new List<Vector2>();
        for (float laserAngle = 0; laserAngle < 2 * Mathf.PI; laserAngle += 2 * Mathf.PI / 1000)
        {
            RaycastHit hit;
            Vector3 origin = transform.position;
            Vector3 direction = new Vector3(Mathf.Sin(laserAngle), Mathf.Cos(laserAngle), 0);
            Ray ray = new Ray(origin, direction);

            if (Physics.Raycast(ray, out hit, laserDistance))
            {
                laserStart = origin;
                laserEnd.Add(hit.point);
                mapper.AddObstacle(hit.transform.gameObject);
            }
            else
            {
                laserEnd.Add(origin + new Vector3(Mathf.Sin(laserAngle), Mathf.Cos(laserAngle), 0) * laserDistance);
            }
        }
    }
}
