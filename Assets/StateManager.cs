﻿using UnityEngine;
using System.Collections;

public class StateManager : MonoBehaviour
{
    Agent agent;
    LaserScanner laserscanner;
    Camera vehicle_cam;
    World world;
    Ackermann ackermann;

    bool dheu = false;

    // Use this for initialization
    void Start()
    {
        agent = GameObject.Find("Agent").GetComponent(typeof(Agent)) as Agent;
        laserscanner = GameObject.Find("LaserScanner").GetComponent(typeof(LaserScanner)) as LaserScanner;
        vehicle_cam = GameObject.Find("vehicle_cam").GetComponent(typeof(Camera)) as Camera;
        world = GameObject.Find("World").GetComponent(typeof(World)) as World;
        ackermann = GameObject.Find("Ackermann").GetComponent(typeof(Ackermann)) as Ackermann;
    }

    // Update is called once per frame
    int view = 0;
    int guion = 1;
    void Update()
    {
        if (agent.state == AgentState.Manual)
        {
            float steering = 0;
            float speed = 0;
            if (Input.GetKey("left"))
            {
                steering += 10;
            }
            if (Input.GetKey("right"))
            {
                steering -= 10;
            }
            if (Input.GetKey("down"))
            {
                speed -= 3;
            }
            if (Input.GetKey("up"))
            {
                speed += 3;
            }
            agent.driver.setManual(steering, speed);
        }

        if (Input.GetKeyDown("v"))
        {
            if (view == 0)
            {
                view = 1;
                vehicle_cam.rect = new Rect(0, 0, 1, 1);
            }
            else
            {
                view = 0;
                vehicle_cam.rect = new Rect(0, 0, 0, 0);
            }
        }

        if (Input.GetKeyDown("m"))
        {
            guion = (guion + 1) % 5;
        }
    }

    void restart_gui()
    {
        if (GUI.Button(new Rect(10, 10, 80, 20), "(RE)Start"))
        {
            if (agent.state == AgentState.Manual)
            {
                agent.state = AgentState.Driving;
            }
            else
            {
                agent.state = AgentState.PathFinding;
                agent.pathfinder.done = false;
            }
        }

        if (GUI.Button(new Rect(100, 10, 80, 20), "Reset"))
        {
            //agent.state = AgentState.Manual;
            //ackermann.transform.position = new Vector3(3, 9, 0);
            //ackermann.transform.eulerAngles = new Vector3(0, 0, 0);
            Application.LoadLevel(0);
        }
    }

    void OnGUI()
    {
        GUI.color = Color.black;
        if (guion == 1)
        {
            //GUI.Box(new Rect(10, 10, 140, 170), "Menu");
            restart_gui();

            if (GUI.Button(new Rect(10, 40, 80, 20), "Stop"))
            {
                agent.state = AgentState.Manual;
            }

            if (GUI.Button(new Rect(10, 70, 80, 20), "Prazno"))
            {
                world.build_world(0);
                agent.mapper.reset();
            }

            if (GUI.Button(new Rect(10, 100, 80, 20), "Ovire"))
            {
                world.build_world(1);
                agent.mapper.reset();
            }

            //if (GUI.Button(new Rect(20, 160, 80, 20), "Map2"))
            //{
            //    world.build_world(2);
            //    agent.mapper.reset();
            //    ackermann.transform.position = new Vector2(35, 20);
            //}

            agent.driver.speed = GUI.HorizontalSlider(new Rect(10, 130, 80, 20), agent.driver.speed, 0.0F, 10.0F);
            GUI.TextArea(new Rect(100, 130, 70, 20), "Hitrost: ", 30);
            GUI.TextArea(new Rect(170, 130, 30, 20), agent.driver.speed.ToString(), 30);
        }
        else if (guion == 2)
        {
            restart_gui();
            agent.pathfinder.position_cells = GUI.HorizontalSlider(new Rect(10, 40, 80, 20), agent.pathfinder.position_cells, 0.5F, 10.0F);
            GUI.TextArea(new Rect(100, 40, 70, 20), "Pozicija: ", 30);
            GUI.TextArea(new Rect(170, 40, 30, 20), agent.pathfinder.position_cells.ToString(), 30);
            agent.pathfinder.heading_cells = (int)GUI.HorizontalSlider(new Rect(10, 70, 80, 20), (float)agent.pathfinder.heading_cells, 1.0F, 64.0F);
            GUI.TextArea(new Rect(100, 70, 70, 20), "Kot: ", 30);
            GUI.TextArea(new Rect(170, 70, 30, 20), agent.pathfinder.heading_cells.ToString(), 30);
            agent.pathfinder.reverse_cell = GUI.Toggle(new Rect(10, 100, 80, 20), agent.pathfinder.reverse_cell, "Vzvratno");
            agent.pathfinder.heu_type = (int)GUI.HorizontalSlider(new Rect(10, 130, 80, 20), (float)agent.pathfinder.heu_type, 0.0F, 2.0F);
            switch (agent.pathfinder.heu_type)
            {
                case 0:
                    GUI.TextArea(new Rect(100, 130, 100, 20), "Brez", 30);
                    break;
                case 1:
                    GUI.TextArea(new Rect(100, 130, 100, 20), "Manhattan", 30);
                    break;
                default:
                    GUI.TextArea(new Rect(100, 130, 100, 20), "Dijkstra", 30);
                    break;
            }
            dheu = GUI.Toggle(new Rect(10, 160, 80, 20), dheu, "Hevristika");
        }
        else if (guion == 3)
        {
            restart_gui();
            agent.pathfinder.lstep = GUI.HorizontalSlider(new Rect(10, 40, 80, 20), agent.pathfinder.lstep, 0.0F, 3.0F);
            GUI.TextArea(new Rect(100, 40, 70, 20), "Premik: ", 30);
            GUI.TextArea(new Rect(170, 40, 30, 20), agent.pathfinder.lstep.ToString(), 30);
            agent.pathfinder.show_steps = (int)GUI.HorizontalSlider(new Rect(10, 70, 80, 20), (float)agent.pathfinder.show_steps, 1.0F, 200.0F);
            GUI.TextArea(new Rect(100, 70, 70, 20), "Korak: ", 30);
            GUI.TextArea(new Rect(170, 70, 30, 20), agent.pathfinder.show_steps.ToString(), 30);
        } else if (guion == 4)
        {
            restart_gui();
            agent.pathfinder.direction_change_cost = GUI.HorizontalSlider(new Rect(10, 40, 80, 20), agent.pathfinder.direction_change_cost, 0.0F, 10.0F);
            GUI.TextArea(new Rect(100, 40, 70, 20), "Spr. smeri: ", 30);
            GUI.TextArea(new Rect(170, 40, 30, 20), agent.pathfinder.direction_change_cost.ToString(), 30);
            agent.pathfinder.reverse_cost = GUI.HorizontalSlider(new Rect(10, 70, 80, 20), agent.pathfinder.reverse_cost, 0.0F, 10.0F);
            GUI.TextArea(new Rect(100, 70, 70, 20), "Vzvratno: ", 30);
            GUI.TextArea(new Rect(170, 70, 30, 20), agent.pathfinder.reverse_cost.ToString(), 30);
            agent.pathfinder.steering_change_cost = GUI.HorizontalSlider(new Rect(10, 100, 80, 20), agent.pathfinder.steering_change_cost, 1.0F, 10.0F);
            GUI.TextArea(new Rect(100, 100, 70, 20), "Spr. kota: ", 30);
            GUI.TextArea(new Rect(170, 100, 30, 20), agent.pathfinder.steering_change_cost.ToString(), 30);
            agent.pathfinder.steering_cost = GUI.HorizontalSlider(new Rect(10, 130, 80, 20), agent.pathfinder.steering_cost, 0.0F, 10.0F);
            GUI.TextArea(new Rect(100, 130, 70, 20), "Kot: ", 30);
            GUI.TextArea(new Rect(170, 130, 30, 20), agent.pathfinder.steering_cost.ToString(), 30);
        }
    }

    public void OnRenderObject()
    {
        Material lineMaterial;
        var shader = Shader.Find("Hidden/Internal-Colored");
        lineMaterial = new Material(shader);
        lineMaterial.hideFlags = HideFlags.HideAndDontSave;
        // Turn on alpha blending
        lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        // Turn backface culling off
        lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        // Turn off depth writes
        lineMaterial.SetInt("_ZWrite", 0);
        lineMaterial.SetPass(0);
        GL.PushMatrix();

        GL.Begin(GL.LINES);
            GL.Color(new Color(1,0,0,(float)0.1));
            for (int i = 0; i < laserscanner.laserEnd.Count; ++i)
            {
                Vector3 s = laserscanner.laserStart;
                Vector3 e = laserscanner.laserEnd[i];
                GL.Vertex3(s.x, s.y, (float)1.5);
                GL.Vertex3(e.x, e.y, (float)1.5);
            }
        GL.End();
        agent.pathfinder.drawPath(dheu);
        GL.PopMatrix();
    }
}
