﻿using UnityEngine;
using System.Collections.Generic;

public class Driver
{
    Ackermann ackermann;
    List<PathFinder.Path> referencePath;
    int drivingIndex;
    int pathIndex;

    public float speed;

    public void setPath(List<PathFinder.Path> referencePath)
    {
        this.referencePath = referencePath;
        drivingIndex = 0;
        pathIndex = 0;
    }

    public void stop()
    {
        ackermann.setControls(0, 0);
    }


    public bool drive()
    {
        if (pathIndex >= referencePath.Count)
        {
            return true;
        }
        else if (drivingIndex >= referencePath[pathIndex].positions.Count - 1)
        {
            pathIndex += 1;
            drivingIndex = 0;
        }
        else
        {
            Vector3 frontAxle = ackermann.transform.position;
            if (referencePath[pathIndex].reverse) frontAxle -= Quaternion.Euler(0, 0, ackermann.transform.eulerAngles.z) * Vector3.right * (float)1.5;
            else frontAxle += Quaternion.Euler(0, 0, ackermann.transform.eulerAngles.z) * Vector3.right * (float)1.5;

            float angle;
            Vector3 posR, v1t, v2t;
            while (true)
            {
                if (drivingIndex >= referencePath[pathIndex].positions.Count - 1) return false;
                v1t = referencePath[pathIndex].positions[drivingIndex];
                v2t = referencePath[pathIndex].positions[drivingIndex + 1];
                angle = Vector3.Angle((v2t - v1t), Vector3.right) * Mathf.Deg2Rad;
                if (Vector3.Cross(v2t - v1t, Vector3.right).z > 0) angle = -angle;
                Vector3 pos0 = frontAxle - v1t;
                posR = Quaternion.Euler(0, 0, -angle * Mathf.Rad2Deg) * pos0;
                if (posR.x > (v2t - v1t).magnitude)
                {
                    drivingIndex += 1;
                }
                else
                {
                    break;
                }
            }

            float heading = ackermann.transform.eulerAngles.z * Mathf.Deg2Rad;
            if (referencePath[pathIndex].reverse) heading += Mathf.PI;
            float steeringA = (angle - heading);
            while (steeringA < -Mathf.PI) steeringA += Mathf.PI * 2;
            while (steeringA > Mathf.PI) steeringA -= Mathf.PI * 2;

            float s = stanley(steeringA, posR.y);
            //s = bangbang(posR.y);
            //if (pathIndex % 2 == 0)
            if (referencePath[pathIndex].reverse)
                ackermann.setControls(-s, -speed);
            else
                ackermann.setControls(s, speed);
        }
        return false;
    }

    public void setManual(float steering, float speed)
    {
        ackermann.setControls(steering, speed);
    }

    public float regulator_k = 1;
    float stanley(float angleDifference, float pathDistance)
    {
        return angleDifference + Mathf.Atan(-pathDistance*regulator_k);
    }

    float bangbang(float pathDistance)
    {
        return -pathDistance;
    }

    public Driver(Ackermann ackermann)
    {
        this.ackermann = ackermann;
    }
}