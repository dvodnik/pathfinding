﻿using UnityEngine;
using System.Collections;

public class Position
{
    public Vector2 position;
    public float heading = 0;

    public Position add(Position that)
    {
        float sin = Mathf.Sin(heading);
        float cos = Mathf.Cos(heading);
        Position added = new Position(position.x + cos * that.position.x - sin * that.position.y, position.y + sin * that.position.x + cos * that.position.y, heading + that.heading);
        return added;
    }

    public Position(Vector2 position, float heading)
    {
        this.position = position;
        this.heading = heading;
    }

    public Position(float x, float y, float heading)
    {
        this.position.x = x;
        this.position.y = y;
        this.heading = heading;
    }
}

public class Ackermann : MonoBehaviour
{
    private GameObject leftWheel, rightWheel, steeringWheel;
    private float steering, speed;
    public readonly float wheelbase = (float)1.5;
    public readonly float maxSteering = (float)0.6;

    // Use this for initialization
    void Start()
    {
        leftWheel = GameObject.Find("Front_L");
        rightWheel = GameObject.Find("Front_R");
        //steeringWheel = GameObject.Find("Steering_wheel_geom");

        //foreach (string name in new string[] {"leftfront", "rightfront", "leftback", "rightback", "front", "back", "leftside", "rightside"}) {
        //    GameObject.Find(name).GetComponent<MeshRenderer>().material.color = Color.black;
        //}
    }

    void Update()
    {
        /* clip steering */
        if (steering < -maxSteering) steering = -maxSteering;
        if (steering > maxSteering) steering = maxSteering;

        /* advance current position */
        Position currentPosition = new Position(transform.position.x, transform.position.y, transform.eulerAngles.z * Mathf.Deg2Rad);
        Position newPosition = currentPosition.add(nextPosition(steering, Time.deltaTime * speed));
        transform.position = new Vector3(newPosition.position.x, newPosition.position.y, 2);
        transform.eulerAngles = new Vector3(0, 0, newPosition.heading * Mathf.Rad2Deg);

        /* move front wheels */
        leftWheel.transform.localEulerAngles = new Vector3(0, 0, steering * Mathf.Rad2Deg);
        rightWheel.transform.localEulerAngles = new Vector3(0, 0, steering * Mathf.Rad2Deg);
        //steeringWheel.transform.localEulerAngles = new Vector3(0, 0, steering * Mathf.Rad2Deg);
    }

    public void setControls(float steering, float speed)
    {
        this.steering = steering;
        this.speed = speed;
    }

    public Position nextPosition(float steering, float distance)
    {
        Position next = new Position(0, 0, 0);
        if (steering == 0)
        {
            next.position.x = distance;
        }
        else if (steering > 0)
        {
            float radius = Mathf.Tan(Mathf.PI / 2 - steering) * wheelbase;
            next.heading = distance / (radius);
            next.position.x = Mathf.Sin(next.heading) * radius;
            next.position.y = (1 - Mathf.Cos(next.heading)) * radius;
        }
        else if (steering < 0)
        {
            float radius = Mathf.Tan(Mathf.PI / 2 + steering) * wheelbase;
            next.heading = distance / (radius);
            next.position.x = Mathf.Sin(next.heading) * radius;
            next.position.y = -(1 - Mathf.Cos(next.heading)) * radius;
            next.heading = -next.heading;
        }
        return next;
    }
}
